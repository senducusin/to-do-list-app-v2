**To-Do List App v2**

This is a sample app that utilizes UIKit, Swift, and CoreData.

---

## Features

- CRUD operation
- Parent-Child data relationship