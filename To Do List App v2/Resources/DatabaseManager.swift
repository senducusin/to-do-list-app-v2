//
//  DatabaseManager.swift
//  To Do List App v2
//
//  Created by Jansen Ducusin on 3/19/21.
//

import UIKit
import CoreData

typealias DMCompletionNullableError = (Error?)->()
typealias DMCompletionGetAllItems = (Result<[Item], Error>)->()
typealias DMCompletionGetAllCategories = (Result<[Category], Error>)->()

class DatabaseManager{
    public let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    static let shared = DatabaseManager()

    private func requestAll<T>(withRequest request:NSFetchRequest<T>, withAttributeSorted attributeName: String?, completion: @escaping((Result<[T], Error>)->())){
        
        if let attributeName = attributeName {
            let attributeSortDescriptor = NSSortDescriptor(key: attributeName, ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare(_:)))
            request.sortDescriptors = [attributeSortDescriptor]
        }
        
        var data:[T]?
        
        do {
            data = try context.fetch(request)
           
        } catch {
            completion(.failure(error))
            return
        }
        
        if let data = data {
            completion(.success(data))
        }
    }
    
    private func requestDelete<T>(withObject object:T, completion: @escaping(DMCompletionNullableError)){
        
        if let object = object as? Item {
            self.context.delete(object)
            self.save(completion: completion)
        } else if let object = object as? Category {
            self.context.delete(object)
            self.save(completion: completion)
        }
    }
    
    private func requestAllWithKeyword<T>(_ keyword: String, withPredicates additionalPredicates:[NSPredicate]?, ofRequest  request:NSFetchRequest<T>, withAttributeSorted attributeName: String, completion: @escaping((Result<[T], Error>)->())) {
        
        /// For more NSPredicate queries, check https://academy.realm.io/posts/nspredicate-cheatsheet/
        /// [cd] stands for 'case' and 'diacritic'  to set query insensitive to both 'case' and 'diacritic'
 
        let predicateSearch = NSPredicate(format: "\(attributeName) CONTAINS[cd] %@", keyword)

        
        var predicates = [NSPredicate]()
        predicates.append(predicateSearch)
        
        if let additionalPredicates = additionalPredicates {
            predicates.append(contentsOf: additionalPredicates)
        }
        
        let compoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        
        let titleSortDescriptor = NSSortDescriptor(key: attributeName, ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare(_:)))

        request.predicate = compoundPredicate
        request.sortDescriptors = [titleSortDescriptor]
        
        var data:[T]?
        
        do {
            data = try context.fetch(request)
           
        } catch {
            completion(.failure(error))
            return
        }
        
        if let data = data {
            completion(.success(data))
        }
        
    }
    
    private func save(completion:@escaping(DMCompletionNullableError)){
        do{
            try context.save()
        }catch{
            
            completion(error)
            return
        }
        
        completion(nil)
    }
}


// MARK: - Public Helpers
extension DatabaseManager{
    
    /// Used for inserting and updating objects, as long as the object/s created or modified are initialized by context^
    public func saveData(completion: @escaping(DMCompletionNullableError)){
        
        do{
            try context.save()
        }catch{
            
            completion(error)
            return
        }
        
        completion(nil)
    }
}


// MARK: - Category Helpers
extension DatabaseManager {
    public func getAllCategories(completion:@escaping(DMCompletionGetAllCategories)){
        let requestCategory: NSFetchRequest<Category> = Category.fetchRequest()
        self.requestAll(withRequest: requestCategory, withAttributeSorted: "name", completion: completion)
    }
    
    public func deleteCategory(withCategory category:Category, completion:@escaping(DMCompletionNullableError)){
        
        self.requestDelete(withObject: category, completion: completion)
    }
}

// MARK: - Item Helpers
extension DatabaseManager {
    
    public func getAllItems(ofCategory category:Category, completion:@escaping(DMCompletionGetAllItems)) {
        
        guard let categoryName = category.name else {
            return
        }
        
        let requestItem: NSFetchRequest<Item> = Item.fetchRequest()
        let query = "category.name MATCHES %@"
        requestItem.predicate = NSPredicate(format: query, categoryName)

        self.requestAll(withRequest: requestItem, withAttributeSorted: "title", completion: completion)
    }
    
    public func deleteItem(withItem item:Item, completion:@escaping(DMCompletionNullableError)){
        
        self.requestDelete(withObject: item, completion: completion)
    }
    
    public func searchItem(withKeyword keyword:String, ofCategory category:Category,  completion:@escaping(DMCompletionGetAllItems)){
        
        guard let categoryName = category.name else {
            return
        }

        let requestItem: NSFetchRequest<Item> = Item.fetchRequest()
        
        let predicateCategory = NSPredicate(format: "category.name MATCHES %@", categoryName)
        
        self.requestAllWithKeyword(keyword, withPredicates: [predicateCategory], ofRequest: requestItem, withAttributeSorted: "title", completion: completion)
    }
}
