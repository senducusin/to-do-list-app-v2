//
//  CategoryTableViewController.swift
//  To Do List App v2
//
//  Created by Jansen Ducusin on 3/20/21.
//

import UIKit

class CategoryTableViewController: UITableViewController {
    // MARK: - Properties
    private var categories = [Category]()
    
    let dbManager = DatabaseManager.shared
    
    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dbManager.getAllCategories { result in
            switch(result){
            
            case .success(let categories):
                self.categories = categories
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        self.setupUI()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let destinationVC = segue.destination as? ToDoListTableViewController,
              let indexPath = tableView.indexPathForSelectedRow else {
            return
        }
        
        destinationVC.selectedCategory = categories[indexPath.row]
        
    }
    
    // MARK: - Selectors
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        var textField:UITextField?
        
        let alert = UIAlertController(title: "Add New To-Do Category", message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Add Category", style: .default) { [weak self] action in
            guard let textField = textField,
                  let newCategoryName = textField.text,
                  !newCategoryName.isEmpty,
                  let dbManager = self?.dbManager else {
                return
            }
            
            let newCategory = Category(context: dbManager.context)
            newCategory.name = newCategoryName
            
            // Saves all objects created with dbManager.context
            DatabaseManager().saveData() { [weak self] error in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                self?.categories.append(newCategory)
                self?.tableView.reloadData()
            }
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addTextField { alertTextfield in
            alertTextfield.placeholder = "Create new item"
            textField = alertTextfield
        }
        
        alert.addAction(action)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Helpers
    private func setupUI(){
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.title = "Categories"
    }
}

extension CategoryTableViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "CategoryTableViewCell", for: indexPath)
        cell.textLabel?.text = categories[indexPath.row].name
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            let category = categories[indexPath.row]
            
            self.dbManager.deleteCategory(withCategory: category) { error in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                
                self.categories.remove(at: indexPath.row)
                self.tableView.reloadData()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "ToDoListTableViewController", sender: self)
        
    }
}
