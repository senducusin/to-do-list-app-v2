//
//  ToDoListTableViewController.swift
//  To Do List App v2
//
//  Created by Jansen Ducusin on 3/19/21.
//

import UIKit


class ToDoListTableViewController: UITableViewController {
    // MARK: - Properties
    private var items = [Item]()
    public var selectedCategory: Category? {
        didSet{
            self.getAllItems()
        }
    }
    
    private let dbManager = DatabaseManager.shared
    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    // MARK: - IBActions
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        var textField:UITextField?
        
        let alert = UIAlertController(title: "Add New To-Do Item", message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Add Item", style: .default) { [weak self] action in
            guard let textField = textField,
                  let newItemTitle = textField.text,
                  !newItemTitle.isEmpty,
                  let dbManager = self?.dbManager,
                  let selectedCategory = self?.selectedCategory else {
                return
            }
            
            /// Adding item to items
            let newItem = Item(context: dbManager.context)
            newItem.title = newItemTitle
            newItem.done = false
            newItem.category = selectedCategory
            
            self?.items.append(newItem)
            
            /// Saves all objects created with dbManager.context
            dbManager.saveData() { [weak self] error in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                
                self?.tableView.reloadData()
            }
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addTextField { alertTextfield in
            alertTextfield.placeholder = "Create new item"
            textField = alertTextfield
        }
        
        alert.addAction(action)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - Helpers
    private func setupUI(){
        self.title = "Items"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.tableView.reloadData()
    }
    
    private func getAllItems(){
        guard let category = selectedCategory else {
            return
        }
        
        self.dbManager.getAllItems(ofCategory: category) { [weak self] result in
            switch(result){
            case .success(let items):
                self?.items = items
                self?.tableView.reloadData()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

extension ToDoListTableViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ToDoListTableViewCell", for: indexPath)
        
        let item = self.items[indexPath.row]

        cell.textLabel?.text = item.title
        cell.accessoryType = item.done ? .checkmark : .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        self.items[indexPath.row].done.toggle()
        
        // Saves the current state of all objects created with DBManager.context
        self.dbManager.saveData() { [weak self] error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            self?.tableView.deselectRow(at: indexPath, animated: true)
            self?.tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            let item = self.items[indexPath.row]
            DatabaseManager.shared.deleteItem(withItem: item) { [weak self] error in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                self?.items.remove(at: indexPath.row)
                self?.tableView.reloadData()
            }
        }
    }
}

extension ToDoListTableViewController: UISearchBarDelegate{

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let keyword = searchBar.text,
              let category = self.selectedCategory else {
            return
        }
        
        if keyword.isEmpty {
            self.getAllItems()
        } else {
            self.dbManager.searchItem(withKeyword: keyword, ofCategory: category) { [weak self] result in
                switch(result){
                   
                case .success(let items):
                    self?.items = items
                    
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                    
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
}
